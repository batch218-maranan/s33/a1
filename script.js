
// #3-4 GET Method

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(object => {
     console.log(object.map(post=> post.title))
 });

// #5-6 Specific GET Method

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(object => {
        console.log(`The item "${object.title}" on the list has a status of "${object.completed}`);
});

// #7 POST Method

fetch('https://jsonplaceholder.typicode.com/todos',{
        method: 'POST',
        headers: {
                'Content-type' : 'application/json'
        },
        body: JSON.stringify({
                title: "Create To Do List Item",
                completed: false,
                userId: 1
        })
})

.then(response => response.json())
.then(json => console.log(json));

// #8-9 PUT Method

fetch('https://jsonplaceholder.typicode.com/todos/1',{
        method: 'PUT',
        headers: {
                'Content-type' : 'application/json'
        },
        body: JSON.stringify({
        		dateCompleted: "Pending",
        		description: "To update the my to do list with a different data structure",
        		status: "Pending",
                title: "Updated To Do List Item",
                userId: 1
        })
})
.then(response => response.json())
.then(json => console.log(json));

// #10-11 PATCH Method

fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PATCH',
        headers: {
                'Content-type' : 'application/json'             
        },
        body: JSON.stringify({
                status : "Complete",
                dateCompleted: "07/09/21"
        })
})
.then(response => response.json())
.then(json => console.log(json));

// #13 Delete Method

fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: "DELETE",
})
.then(response => response.json())
.then(data => console.log(data))